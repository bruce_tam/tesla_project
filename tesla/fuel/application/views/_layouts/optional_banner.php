<?php $this->load->view('_blocks/header')?>
	<?php //echo fuel_nav(array('language' => detect_lang()));?>
	<?php $this->load->view('_blocks/mynav','');?>
	<?php $top_banner_image = fuel_var('top_banner_image','');?>
	<?php if (!empty($top_banner_image)):?>
	<section id="top_banner_image">
		<a href=<?php echo fuel_var('image_link','')?> title="<?php echo fuel_var('image_caption','');?>"> 
			<img src="<?=img_path($top_banner_image)?>"/>
		</a>
	</section>
	<?php endif;?>
	<section id="main_inner">
		<?php echo fuel_var('body', 'This is a default layout. To change this layout go to the fuel/application/views/_layouts/main.php file.'); ?>
	</section>
	
<?php //$this->load->view('_blocks/footer')?>
<?php //$this->load->view('_blocks/footer')?>