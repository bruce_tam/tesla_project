<?php $this->load->view('_blocks/bootstrap_header')?>
	<?php //echo fuel_nav(array('language' => detect_lang()));?>
	<?php $this->load->view('_blocks/bootstrap_nav','');?>
	
	<?php if (!empty($top_banner_image)):?>
	<div id="top_banner_image" class="jumbotron">
		<a href=<?php echo fuel_var('image_link','#')?>> 
			<img style="width:100%;" src="<?=img_path($top_banner_image)?>" title="<?php echo fuel_var('image_caption','');?>"/>
		</a>
	</div>
	<?php endif;?>
	<section id="main_inner">
		<?php echo fuel_var('body', 'This is a default layout. To change this layout go to the fuel/application/views/_layouts/main.php file.'); ?>
	</section>
	
<?php //$this->load->view('_blocks/footer')?>
<?php //$this->load->view('_blocks/footer')?>
