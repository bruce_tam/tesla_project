<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

<div class="collapse navbar-collapse" id="navbar-collapse">
<ul class="nav navbar-nav">
<?php
$nav = fuel_nav(array('render_type'=>'array', 'container_tag_id' => 'topmenu', 'item_id_prefix' => 'topmenu_', 'language' => detect_lang(), 'language_default_group' => TRUE, 'subcontainer_tag_class' => array(0 => 'dropdown-menu')) ); 
		
$page_code = uri_path();
$site_url = site_url('',false,detect_lang()); 
			
			$total_nav = count($nav);
			$cnt_nav = 0;
			foreach($nav as $uri=>$item):
				$cnt_nav++;
				$classes = '';
				$dropdown = '';
				if ($item['location'] == '') {
					$href = '';
				} else {
					$href = "href='$site_url{$item['location']}'";
					if ($item['location'] == $page_code) {
						$classes .='active';
					}
				}
				if(!empty($item['children'])) {
					$classes .= ' dropdown';
					$dropdown = 'class="dropdown-toggle"';
				}
				$span_delimiter = ($total_nav != $cnt_nav) ? "<span class='span_delimiter'>|</span>" : "";
			?>
				<?php if(!empty($item['children'])) :?>
				<li class="<?=$classes?>">
					<a <?=$href?> <?=$dropdown?> data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$item['label']?> <span class="caret"></span></a> 
				<?php else:?>
				<li class="<?=$classes?>">
					<a <?=$href?>><?=$item['label']?></a>
				<?php endif;?>
				<?php if(!empty($item['children'])):?>
					<ul class="dropdown-menu">
					<?php foreach($item['children'] as $suburl=>$sub_item): ?>
						<?php $classes = ($sub_item['location'] == $page_code) ? 'active' : '' ; ?>
						<li class="<?=$classes?>">
							<a href="<?=$site_url?><?=$sub_item['location']?>">
								<?=$sub_item['label']?>
							</a>
						</li>
					<?php endforeach; ?>
					</ul>
				<?php endif; ?>
				</li>
			<?php endforeach; ?>
			

</ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>