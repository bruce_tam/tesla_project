<?php

class About extends CI_Controller {
     
    function __construct()
    {
        parent::__construct();
    }
     
    function contact()
    {
        // set your variables
        $vars = array('page_title' => 'Contact : My Website 2','bruce' => 'testing'); 
 
        //... form code goes here
        $this->fuel->pages->render('about/contact', $vars);
    }
}

?>